
  
  
  

# Create Secret ID, Secret Key

  

  

- Login [https://www.moneyspace.net](https://www.moneyspace.net/)

  

- Choose menu Webhooks

  

- Enter domain : http://**yourwebsite**.**com**

- Enter Webhook : http://**yourwebsite**.**com**/index.php?route=extension/payment/moneyspaceqrprom/webhook

  
  

  

  

  

![enter image description here](https://worldwallet.net/img_bitbucket/domain_webhook_url.jpg)

  

![enter image description here](https://worldwallet.net/img_bitbucket/secret_id_key.jpg)

  
  

****

  

# Requirements

- Opencart 3+

  

# Configuration

  

- Download repository

- Unzip repository file

- Choose menu **Extensions** -> **Installer** -> **Upload File** -> **moneyspace_for_opencart_3.ocmod** or **moneyspace_payment_oc4.ocmod** (for opencart 4) 

- Choose submenu **Extensions** -> **Payments**

- Then press + to install Moneyspace Payment and click edit to setting

  

****

![enter image description here](https://worldwallet.net/img_bitbucket/opencart/Installer.png)

  

![enter image description here](https://worldwallet.net/img_bitbucket/opencart/Upload.png)

  ![enter image description here](https://worldwallet.net/img_bitbucket/opencart/Upload2.png)


  ![enter image description here](https://worldwallet.net/img_bitbucket/opencart/Upload%20success.png)


![enter image description here](https://worldwallet.net/img_bitbucket/opencart/ms%20Extentions.png)
  

- Set Status to Enabled, Enter Secret ID, Secret Key, Responsible fee person, Set Completed Status after payment is completed, then click Save.


****

# Payment Methods

  

- Moneyspace Payment (Pay by Card 3D secured)

- Moneyspace Payment (QR Code PromptPay)

- Moneyspace Payment (ผ่อนชำระรายเดือน)

  

![enter image description here](https://worldwallet.net/img_bitbucket/opencart/ms%20payments.png)

  

# Changelog

-  ##### 2022-09-25 : Add extension for opencart 4

-  ##### 2021-12-23 : Updated installation and documentation

-  ##### 2021-12-22 : Updated readme (images)

-  ##### 2021-06-12 : Updated idpay and fixed bugs.

-  ##### 2020-10-23 : Updated the order status system.

-  ##### 2020-04-11 : Fixed bugs and added installment payments.

-  ##### 2019-12-12 : Fixed bugs and updated qr code promptpay ( qrnone )

-  ##### 2019-10-26 : Add title setting and Responsible fee person

-  ##### 2019-07-27 : Added